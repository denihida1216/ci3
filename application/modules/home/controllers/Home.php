<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Home extends CI_Controller {
 
	public function index()
	{
		$menu = [[
			'id' => '1',
			'name'=>'Master Data',
			'link'=>'#master-data',
			'icon'=>'fa fa-circle-o',
			'group_id'=>'0',
			'number'=>'1',
			'child'=>[
				[
					'id'=>'3',
					'name'=>'Kategori Barang',
					'link'=>'#kategori-barang',
					'icon'=>'fa fa-circle-o',
					'group_id'=>'1',
					'number'=>'1',
					'child'=>[
						[
							'id'=>'7',
							'name'=>'Kategori Barang detail 1',
							'link'=>'#kategori-barang',
							'icon'=>'fa fa-circle-o',
							'group_id'=>'1',
							'number'=>'1',
							'child'=>[
								[
									'id'=>'9',
									'name'=>'Kategori Barang detail 1-1',
									'link'=>'#kategori-barang',
									'icon'=>'fa fa-circle-o',
									'group_id'=>'1',
									'number'=>'1',
									'child'=>[
										[
											'id'=>'11',
											'name'=>'Kategori Barang detail 1-2',
											'link'=>'#kategori-barang',
											'icon'=>'fa fa-circle-o',
											'group_id'=>'1',
											'number'=>'1',
											'child'=>[
												[
													'id'=>'11',
													'name'=>'Kategori Barang detail 1-3',
													'link'=>'#kategori-barang',
													'icon'=>'fa fa-circle-o',
													'group_id'=>'1',
													'number'=>'1',
													'child'=>[
														[
															'id'=>'11',
															'name'=>'Kategori Barang detail 1-4',
															'link'=>'#kategori-barang',
															'icon'=>'fa fa-circle-o',
															'group_id'=>'1',
															'number'=>'1',
															'child'=>[],
														],
														[
															'id'=>'12',
															'name'=>'Kategori Barang detail 2-4',
															'link'=>'#kategori-merk',
															'icon'=>'fa fa-circle-o',
															'group_id'=>'1',
															'number'=>'2',
															'child'=>[],
														],
													],
												],
												[
													'id'=>'12',
													'name'=>'Kategori Barang detail 2-3',
													'link'=>'#kategori-merk',
													'icon'=>'fa fa-circle-o',
													'group_id'=>'1',
													'number'=>'2',
													'child'=>[],
												],
											],
										],
										[
											'id'=>'12',
											'name'=>'Kategori Barang detail 2-2',
											'link'=>'#kategori-merk',
											'icon'=>'fa fa-circle-o',
											'group_id'=>'1',
											'number'=>'2',
											'child'=>[],
										],
									],
								],
								[
									'id'=>'10',
									'name'=>'Kategori Barang detail 2-1',
									'link'=>'#kategori-merk',
									'icon'=>'fa fa-circle-o',
									'group_id'=>'1',
									'number'=>'2',
									'child'=>[],
								],
							],
						],
						[
							'id'=>'8',
							'name'=>'Kategori Barang detail 2',
							'link'=>'#kategori-merk',
							'icon'=>'fa fa-circle-o',
							'group_id'=>'1',
							'number'=>'2',
							'child'=>[],
						],
					],
				],
				[
					'id'=>'4',
					'name'=>'Kategori Merk',
					'link'=>'#kategori-merk',
					'icon'=>'fa fa-circle-o',
					'group_id'=>'1',
					'number'=>'2',
					'child'=>[],
				],
			]
		],
		[
			'id'=>'2',
			'name'=>'Keuangan',
			'link'=>'#keuangan',
			'icon'=>'fa fa-circle-o',
			'group_id'=>'0',
			'number'=>'2',
			'child'=>[
				[
					'id'=>'5',
					'name'=>'Penjualan',
					'link'=>'#penjualan',
					'icon'=>'fa fa-circle-o',
					'group_id'=>'2',
					'number'=>'1',
					'child'=>[],
				],
				[
					'id'=>'6',
					'name'=>'Pembelian',
					'link'=>'#pembelian',
					'icon'=>'fa fa-circle-o',
					'group_id'=>'2',
					'number'=>'2',
					'child'=>[],
				]
			],
		]];
		$data = [
			'menu'=>$this->menu_load($menu),
		];
		// echo json_encode($menu);
		// echo ($this->menu_load($menu));
		$this->load->view('view_home',$data);
	}

	function menu_load($menu)
	{
		$data = '';
		foreach ($menu as $menu) {
			$data.='<li class="treeview" title="'.$menu['name'].'">';
			$data.='<a href="'.$menu['link'].'">
						<i class="'.$menu['icon'].'"></i> <span>'.$menu['name'].'</span>';
			if ($menu['child']){
				$data.='<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">';
				foreach ($menu['child'] as $child) {
					if ($child['child']){
						$data.='<li class="treeview" title="'.$child['name'].'">
									<a href="'.$child['link'].'">
										<i class="'.$child['icon'].'"></i> '.$child['name'].'
										<span class="pull-right-container">
											<i class="fa fa-angle-left pull-right"></i>
										</span>
									</a>
								<ul class="treeview-menu">';
						$data.=$this->menu_load($child['child']);
						$data.='</ul></li>';
					}else{
						$data.='<li title="'.$child['name'].'">
									<a href="'.$child['link'].'">
										<i class="'.$child['icon'].'"></i> '.$child['name'].'
									</a>
								</li>';
					}
				}
				$data.='</ul>';
			}else{
				$data.='</a>';
			}
			$data.='</li>';
		}
		return $data;
	}
}